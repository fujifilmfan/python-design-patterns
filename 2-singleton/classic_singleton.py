#!/usr/bin/python


class Singleton(object):
    def __new__(cls):
        # before creating instance, check for the __new__ method that is
        # called right before __init__ if we had created the instance
        # earlier; if not, create new instance and return it; otherwise, return
        # already
        # created instance
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance

    only_one_var = "I'm only one var"


class Child(Singleton):
    pass


"""
# Use:
>>> from classic_singleton import Singleton
>>> singleton1 = Singleton()
>>> singleton2 = Singleton()
>>> singleton1 is singleton2
True
>>> singleton1.only_one_var
"I'm only one var"
>>> singleton2.only_one_var
"I'm only one var"
>>> from classic_singleton import Child
>>> child = Child()
# The book says that the following should not work:
>>> child is singleton1
True
>>> child.only_one_var
"I'm only one var"
>>> print child.only_one_var
I'm only one var
>>> print singleton1
<classic_singleton.Singleton object at 0x107f30490>
>>> print singleton2
<classic_singleton.Singleton object at 0x107f30490>
>>> print child
<classic_singleton.Singleton object at 0x107f30490>
# Oh, it's an error in the book.  It should say that this shouldn't work if 
the child is instantiated first
>>> from classic_singleton import Singleton, Child
>>> child = Child()
>>> child.only_one_var
"I'm only one var"
>>> singleton = Singleton()
>>> child is singleton
False
>>> child.only_one_var
"I'm only one var"
>>> singleton.only_one_var
"I'm only one var"
# They're not the same object, but they both do have the attribute
"""
