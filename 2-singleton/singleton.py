#!/usr/bin/python

import httplib2
import os
# import re
import threading
import urllib
from urlparse import urlparse, urljoin
from BeautifulSoup import BeautifulSoup


# returns singleton objects to all parts of the code that request it; see
# classic_singleton.py, also
class Singleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance

    downloaded = set()
    to_visit = set()


# creates a thread, which downloads images from the website
class ImageDownloaderThread(threading.Thread):
    """A thread for downloading images in parallel."""
    def __init__(self, thread_id, name, counter):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        print 'Starting thread ' + self.name
        download_images(self.name)
        print 'Finished thread ' + self.name + '/n'


# traverses website using BFS algorithm, finds links, and adds them to a
# set for further downloading; specify maximum links to follow
def traverse_site(max_links=10):
    link_parser_singleton = Singleton()

    # while we have pages to parse in queue
    while link_parser_singleton.queue_to_parse:
        # if collected enough links to download images, return
        if len(link_parser_singleton.to_visit) == max_links:
            return

        url = link_parser_singleton.queue_to_parse.pop()

        http = httplib2.Http()
        try:
            status, response = http.request(url)
        except Exception:
            continue

        # skip if not a web page
        if 'text/html' not in status.get('content-type'):
           continue

        # the following original code did not work because the actual
        # content-type was 'text/html; charset=UTF-8'; I used a print
        # statement to determine this
        # if status.get('content-type') != 'text/html':

        # add the link to queue for downloading images
        link_parser_singleton.to_visit.add(url)
        print 'Added', url, 'to queue'

        bs = BeautifulSoup(response)

        for link in BeautifulSoup.findAll(bs, 'a'):

            link_url = link.get('href')

            # <img> tag may not contain href attribute
            if not link_url:
                continue

            parsed = urlparse(link_url)

            # if link follows to external webpage, skip it
            if parsed.netloc and parsed.netloc != parsed_root.netloc:
                continue

            # Construct a full url from a link which can be relative
            link_url = (parsed.scheme or parsed_root.scheme) + '://' + (
                parsed.netloc or parsed_root.netloc) + parsed.path or ''

            # if link was added previously, skip it
            if link_url in link_parser_singleton.to_visit:
                continue

            # add a link for further parsing
            link_parser_singleton.queue_to_parse = [link_url] + \
                                                   link_parser_singleton.queue_to_parse


# downloads images from last web resource page in the singleton.to_visit
# queue and saves it to the img directory; here, we use a singleton for
# synchronizing shared data, which is a set of pages to visit between two
#  threads
def download_images(thread_name):
    singleton = Singleton()
    # while we have pages where we have not downloaded images
    while singleton.to_visit:
        url = singleton.to_visit.pop()

        http = httplib2.Http()
        print thread_name, 'Starting downloading images from', url

        try:
            status, response = http.request(url)
        except Exception:
            continue

        bs = BeautifulSoup(response)

        # find all <img> tags
        images = BeautifulSoup.findAll(bs, 'img')

        for image in images:
            # get image source url which can be absolute or relative
            src = image.get('src')
            # construct a full url; if the image url is relative, it will
            #  be prepended with webpage domain; if image url is
            # absolute, it will remain as is
            src = urljoin(url, src)

            # get a base name, for example 'image.png' to name file locally
            basename = os.path.basename(src)

            if src not in singleton.downloaded:
                singleton.downloaded.add(src)
                print 'Downloading', src
                # download image to local filesystem
                urllib.urlretrieve(src, os.path.join('images', basename))

        print thread_name, 'finished downloading images from', url


if __name__ == '__main__':
    root = 'https://mixedupalot.wordpress.com/'
    # root = 'http://python.org'

    parsed_root = urlparse(root)

    singleton = Singleton()
    singleton.queue_to_parse = [root]
    # a set of urls to download images from; I made this part of the class
    # definition
    # singleton.to_visit = set()

    # downloaded images; I made this part of the class definition
    # singleton.downloaded = set()

    traverse_site()

    # create images directory if it doesn't exist
    if not os.path.exists('images'):
        os.makedirs('images')

    # create new threads
    thread1 = ImageDownloaderThread(1, "Thread-1", 1)
    thread2 = ImageDownloaderThread(2, "Thread-2", 2)

    # start new threads
    thread1.start()
    thread2.start()
