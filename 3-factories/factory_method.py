#!/usr/bin/python

import abc
import urllib2
from BeautifulSoup import BeautifulStoneSoup


class Connector(object):
    """Abstract class to connect to remote resource. It provides two Factory
    Methods to be implemented by its subclasses: protocol_factory_method and
    abstract_factory_method"""
    __metaclass__ = abc.ABCMeta  # Declares class as abstract class
    # ? What is an abstract class?

    def __init__(self, is_secure):
        self.is_secure = is_secure
        self.port = self.port_factory_method()
        self.protocol = self.protocol_factory_method()

    @abc.abstractmethod
    def parse(self):
        """Parses web content.  This method should be redefined in the
        runtime."""
        # ? What does this do?  A: The method is implemented in subclasses.
        pass

    def read(self, host, path):
        """A generic method for all subclasses, reads web content."""
        url = self.protocol + '://' + host + ':' + str(self.port) + path
        print 'Connecting to ', url
        return urllib2.urlopen(url, timeout=2).read()

    @abc.abstractmethod
    def protocol_factory_method(self):
        """A factory method that must be redefined in subclass."""
        pass

    @abc.abstractmethod
    def port_factory_method(self):
        """Another factory method that must be redefned in subclass."""
        # ? What does this do?
        # ? Why not a 'pass' here? The program does work with a pass instead.
        #  The errata for the book says that this should be 'pass'.
        # return FTPPort()
        pass


class HTTPConnector(Connector):
    """A concrete creator that creates a HTTP connector and sets in runtime
    all its attributes. It implements the Factory Methods."""
    def protocol_factory_method(self):
        if self.is_secure:
            return 'https'
        # ? Why no 'else' statement?
        return 'http'

    def port_factory_method(self):
        """Here HTTPPort and HTTPSecurePort are concrete objects, created by
        factory method."""
        if self.is_secure:
            return HTTPSecurePort()
        return HTTPPort()

    def parse(self, content):
        """Parses web content."""
        filenames = []
        soup = BeautifulStoneSoup(content)
        links = soup.table.findAll('a')
        for link in links:
            filenames.append(link['href'])
        return '\n'.join(filenames)


class FTPConnector(Connector):
    """A concrete creator that creates a FTP connectory and sets in runtime
    all its attributes. It implements the Factory Methods."""
    def protocol_factory_method(self):
        return 'ftp'

    def port_factory_method(self):
        return FTPPort()

    def parse(self, content):
        lines = content.split('\n')
        filenames = []
        for line in lines:
            # The FTP format typically has 8 columns, split them
            splitted_line = line.split(None, 8)
            if len(splitted_line) == 9:
                filenames.append(splitted_line[-1])
        return '\n'.join(filenames)


class Port(object):
    """Abstract product. One of its subclasses will be created in factory
    methods. An interface for our products."""
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __str__(self):
        pass


# The three subclasses implementing the interface follow:
class HTTPPort(Port):
    """A concrete product which represents http port."""
    def __str__(self):
        return '80'


class HTTPSecurePort(Port):
    """A concrete product which represents https port."""
    def __str__(self):
        return '443'


class FTPPort(Port):
    """A concrete product which represents ftp port."""
    def __str__(self):
        return '21'


# Our client code that determines which Creator class to instantiate:
if __name__ == '__main__':
    domain = 'ftp.freebsd.org'
    path = '/pub/FreeBSD/'

    protocol = input('Connecting to {}.  Which Protocol to use? (0-http, '
                     '1-ftp): '.format(domain))

    if protocol == 0:
        is_secure = bool(input('Use secure connection? (1-yes, 0-no): '))
        connector = HTTPConnector(is_secure)
    else:
        is_secure = False
        connector = FTPConnector(is_secure)

    try:
        content = connector.read(domain, path)
    except urllib2.URLError, e:
        print 'Can not access resource with this method'
    else:
        print connector.parse(content)
