Learning Python Design Patterns
=====

1 Model-View-Controller
-----

### Introduction
* model is the data and business logic
* view is the window on the screen (presentation)
* controller is the glue between the two
* view and controller depend on the model, but model is independent of them
User --uses--> Controller --manipulates--> Model --returns data--> Controller --updates--> View --renders--> User
* We need *smart* models, *thin* controllers, and *dumb* views.

### Model - the knowledge of the application
* provides knowledge: data and how to work with that data
* has a state and methods for changing its state  
when working with models:
* do:  
  * create data models and interface of work them
  * validate data and report all errors to the controller
* avoid working directly with the user interface

### View - the appearance of knowledge
* receives data from model through controller
* should not contain complex logic  
when working with views:
* do: try to keep them simple; use only simple comparisons and loops
* avoid:  
  * accessing the database directly
  * using any logic other than loops and conditional statements

### Controller - the glue between the model and view
* should be "thin"  
when working with controllers:
* do:  
  * pass data from user requests to the model for processing, retrieving, and saving data
  * pass data to views for rendering
  * handle all request errors and errors from models
* avoid:
  * rendering data
  * work with the database and business logic directly

### Benefits of using the MVC
* loose coupling and decreased complexity
* change visualization without changing the business logic
* change the business logic without changing the visualization
* change response to a user action via a different controller

### Implementation in Python
* create a URL shortening service with a Flask micro framework
* See code in 1-mvc/

### Summary
* model about knowledge, data, and business logic
* view is about presentation to end users
* controller is glue between model and view; keep it thin

2 Creating Only One Object with the Singleton Pattern
-----

### Introduction
* the **singleton** is used to create only one instance of data, whether a class, list, dictionary, etc.
* singleton is best for the following requirements:
  * you need to control concurrent access to a shared resource
  * you need a global point of access for the resource from multiple or different parts of the system
  * you need to have only one object
* typical use cases:
  * logging class and its subclasses (global point of access for the logging class to send messages to log)
  * printer spooler (to avoid conflicting requests for the same resource)
  * managing a connection to a database
  * file manager
  * retrieving and storing information on external configuration files
  * read-only singletons storing some global states (user language, time, time zone, application path, etc.)

### A module-level singleton
All modules are singletons by nature because of Python's module importing steps:
1. check whether a module is already imported
2. if yes, return it
3. if not, find a module, initialize it, and return it
4. initializing a module means executing code, including all module-level assignments
  * when importing module for first time, all initializations are done
  * on the second try, Python will return the initialized module
* Sample code in 2-singleton/ (`module_singleton.py`, `module1.py`, `module2.py`)
* `module2.py` receives the changed variable from `module1.py` (but only on import from within `module1.py` and only when the import is after the variable change
* some points:
  * it's error-prone; e.g., if you forget the global statements, variables local to the function will be created, and the modules's variables won't be changed
  * it's ugly, esp. if there are many objects that should remain singletons
  * it pollutes the module namespace w/ unnecessary variables
  * no lazy allocation and initialization; all global variables will be loaded during module import process
  * can't reuse the code b/c you cannot use the inheritance
  * no special methods and no OOP benefits at all

### A classic singleton
In a classic singleton in Python, we check whether an instance is already created; if it is, we return it; otherwise, we create a new instance, assign it to a class attribute, and return it
* Sample code in 2-singleton/ (`classic_singleton.py`)
* all created instances are the same instance
* the book says this doesn't work with subclasses, but it did for me

### The borg singleton
In the borg pattern, all of the instances are different but share the same state; also known as **monostate**
* Sample code in 2-singleton/ (`borg_singleton.py`)
* use instead of classic when you expect the singleton to be inherited (or choose classic if you think it won't be)

### Implementation in Python
* create a web scraper than scans for links, follows them, and downloads images it finds
* use two threads to make it quicker, but to prevent interference with each other, use a shared resource
Problems:
* I had the indentation wrong at first (it was hard to tell from the book)
* There was a bug in the book code:
```python
# skip if not a web page
if 'text/html' not in status.get('content-type'):
   continue

# the following original code did not work because the actual
# content-type was 'text/html; charset=UTF-8'; I used a print
# statement to determine this
# if status.get('content-type') != 'text/html':
```

### Summary
A singleton is a design pattern for creating only one instance of a class. Modules in Python are singletons by nature. A classic singleton checks whether the instance was created earlier; if not, it creates and returns it. The Borg singleton uses shared state for all objects. In the example shown in the chapter, we used the Singleton class for accessing a shared resource and a set of URLs to fetch images from, and both threads used it to properly parallelize their work.

3 Building Factories to Create Objects
-----

### Introduction
A factory is a class for creating other objects. Usually this class has methods that accept some parameters and returns some type of object depending on the parameters passed.  
In this chapter:
* How to create a simple factory
* What the Factory Method is, when to use it, and how to implement it for building a tool that can be connected to a variety of web resources
* What the Abstract Factory is, when to use it, and how it is different from the Factory method pattern
Why use factories instead of direct object instantiation? (? I'm not sure what the question means - I expect to find out.)
* Factories provide loose coupling, separating object creation from using specific class implementation.
* A class that uses the created object does not need to know exactly which class is created. All it needs to know is the created class's interface, that is, which created class's methods can be called and with which arguments. Adding
new classes is done only in factories as long as the new classes comply with the interface, without modifying the client code.
* The Factory class can reuse existing objects, while direct instantiation always creates a new object.
* In **simple_factory.py**, **the factory is the SimpleFactory class, which has a static method, build_connection.  You pass it an argument (a type of protocol) and the factory constructs and returns an object depending on the passed argument.** So, the client code is not responsible anymore for object creation; it just uses the object generated by the factory without knowing exactly which object was generated as long as the generated object implements some interface.
Factory is not a design pattern by itself; rather, it's a concept that serves as a basis for several design patterns such as Factory Method and Abstract Factory.
```python
class SimpleFactory(object):
    # the @staticmethod decorator allows us to run the method without a class
    #  instance, e.g., SimpleFactory.build_connection
    @staticmethod
    def build_connection(protocol):
        if protocol == 'http':
            return HTTPConnection()
        elif protocol == 'ftp':
            return FTPConnection()
        else:
            raise RuntimeError('Unknown protocol')


if __name__ == '__main__':
    protocol = raw_input('Which Protocol to use? (http or ftp): ')
    protocol = SimpleFactory.build_connection(protocol)
    protocol.connect()
    print protocol.get_response()
```

### The Factory Method
**The essence of this pattern is to define an interface for creating an object, but let the classes that implement the interface decide which class to instantiate.**

### Advantages of using the Factory Method pattern
* makes code more universal, not being tied to concrete classes (ConcreteProduct) but to interfaces (Product) providing low coupling. It separates interfaces from their implementations
* decouples the code that creates objects from the code that uses them, reducing the complexity of maintenance; to add a new class, you need to add an additional else-if clause


### The Factory Method implementation
* create a tool for accessing web resources using HTTP or FTP
  * `Creator` abstract class named `Connector`; makes connection, reads response, parses responses; child classes decide which port and protocol to use; `some_operation()` in the example is probably `read()` in this implementation:
    * "The some_operation method does not care which object is created as long as it implements the Product interface and provides the implementation for all methods in that interface."
    * `read()` builds a URL as long as the created objects return a host, path, etc.
  * ? is `HTTPConnector` or `Port` a `ConcreteCreator` in this scenario? I think the former since it is `HTTPConnector` and `FTPConnector` that redefine the factory methods at runtime
  * the two ports will be `ConcreteProducts`
  * "So, the responsibility of making decisions about which port to instantiate was moved to the subclasses of the Connection class, decoupling the method that uses the connection (`read`) from the method that creates it (`port_factory_method`)."
* See **factory_method.py**  
* How the code works:  
1. The user selects the protocol from prompt; stored as `protocol`
2. If the user selects 'HTTP', then they also get a prompt for setting whether the connection will be secure; stored as `is_secure`
3. If 'HTTP', then `connector = HTTPConnector(is_secure)`
4. HTTPConnector has three methods, **protocol_factory_method** (returns https or http depending on value of `is_secure`), **port_factory_method** (returns either HTTPSecurePort() or HTTPPort()), and **parse**; I'll assume `HTTP` moving forward
5. After user selections, there is the try statement: `content = connector.read(domain, path)`
6. The **read** method belongs to the parent class, **Connector**
  * it calls `self.protocol`, which is set to `self.protocol.factory_method()` in Connector and passes in Connector but is set to 'http' or 'https' in HTTPConnector
  * it calls `self.port`, which is set to `self.port_factory_method()`, which return FTPPort() in Connector but HTTPPort() in HTTPConnector, and HTTPPort returns '80'
7. **read** constructs the URL and then returns the result of urllib2.urlopen(url, timeout=2).read()
8. ? How does anything else happen after that?  Ah, the else statement runs
9. The else statement causes parsed content to be printed to the console

### Abstract Factory
If the goal of the **Factory Method** is to move instances creating to subclasses, the goal of an **abstract** factory is to create families of related objects without depending on their specific classes.  
**Abstract Factory** is used when you need to create a family of objects that do some work together.  
The benefit of using **Abstract Factory** is that it isolates the creation of objects from the client that needs them, giving the client only the possibility of accessing them through an interface, which makes the manipulation easier. If the products of a family are meant to work together, the AbstractFactory class makes it easy to use the objects from only one family at a time.  

### Advantages of using the Abstract Factory pattern
* it simplifies the replacement of product families
* it ensures the compatibility of the products in the product's family
* it isolates the concrete classes from the client

### Abstract Classes and Methods
* https://www.python-course.eu/python3_abstract_classes.php
* Abstract classes are classes that contain one or more abstract methods. An abstract method is a method that is declared, but contains no implementation. Abstract classes may not be instantiated, and require subclasses to provide implementations for the abstract methods.
* An abstract method can have an implementation in the abstract class! Even if they are implemented, designers of subclasses will be forced to override the implementation. Like in other cases of "normal" inheritance, the abstract method can be invoked with super() call mechanism. This makes it possible to provide some basic functionality in the abstract method, which can be enriched by the subclass implementation.
* simple example:
```python
from abc import ABC, abstractmethod
 
class AbstractClassExample(ABC):
    
    @abstractmethod
    def do_something(self):
        print("Some implementation!")
        
class AnotherSubclass(AbstractClassExample):
    def do_something(self):
        super().do_something()
        print("The enrichment from AnotherSubclass")
        
x = AnotherSubclass()
x.do_something()
```  
> Some implementation!  
> The enrichment from AnotherSubclass  

### Abstract Factory implementation
See **abstract_factory.py**  

### Abstract Factory versus Factory Method
* Use the Factory Method pattern when there is a need to decouple a client from a particular product it uses. Use the Factory Method to relieve a client of the responsibility of creating and configuring instances of a product.
* Use the Abstract Factory pattern when clients must be decoupled from the product classes. The Abstract Factory pattern can also enforce constraints specifying which classes must be used with others, creating independent families of objects.

### Summary
In object-oriented development terminology, a **factory** is a class for creating other classes.  
The **Factory Method** defines an interface for creating an object, but lets the classes that implement the interface decide which class to instantiate. The Factory Method makes code more universal, not being tied to concrete classes but to interfaces.  
**Abstract Factory** provides an interface for creating families of related or dependent objects without specifying their concrete classes. It simplifies the replacement of product families and ensures the compatibility of the products consisting in the product family.


